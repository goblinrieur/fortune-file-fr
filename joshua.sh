#! /usr/bin/env bash 

function text_to_speech() 
{
	espeak -v mb/mb-fr1 "$1" -s 150 2> /dev/null
}

function installer()
{
	echo Using sudo to install needed packages 
	sudo apt-get update
	sudo apt-get install -y espeak ansifilter espeak-data speech-dispatcher-espeak docker
}

function install()
{
	cp "$0" "${HOME}"
	echo "${HOME}/$0" | sed 's/\.\///'>> ~/.bashrc
}
if ! grep -i joshua ~/.bashrc ; then install ; fi
if ! which espeak > /dev/null ; then 
	echo espeak not installed
	echo running install
	installer
fi
if [ -f ./soundrescue.sh ] ; then ./soundrescue.sh ; fi
clear
Message=$(cat <(docker run francoispussault/fortunes:latest | ansifilter | sed 's/-+-/-------------!/g;s/#.*\ /\ /'))
echo "${Message}"&
text_to_speech "${Message}"
exit 0
