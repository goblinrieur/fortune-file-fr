# fortune file FR 

```
        ___   ___   ___  
__   __/ _ \ / _ \ / _ \ 
\ \ / / | | | | | | | | |
 \ V /| |_| | |_| | |_| |
  \_/  \___(_)___(_)___/ 
                         
```

# goal

This is just a toy run while calling .bashrc running fortune (fr) but also using a text to speech of the fortune message on audio.

This project might have no or very few updates & should remain untouched for a while. And it is currently only compatible to bash & Debian familly distos.

# demo 

[video](https://youtu.be/WGjZ0k4gtIA)


